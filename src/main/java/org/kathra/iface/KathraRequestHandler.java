/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.iface;

import com.google.common.io.Files;
import org.kathra.utils.Session;
import org.kathra.utils.KathraException;
import org.kathra.utils.KathraSessionManager;
import io.netty.handler.codec.http.multipart.*;
import org.apache.camel.Exchange;
import org.apache.camel.component.netty4.NettyConstants;
import org.apache.camel.component.netty4.http.NettyHttpMessage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public interface KathraRequestHandler extends KathraSessionManager, KathraExceptionHandler {

    String HTTP_SERVER = "netty4-http";

    default Session createNewSession(Exchange exchange) {
        Session session = new Session();
        session.id(exchange.getExchangeId());
        try {
            InetSocketAddress nettyRemoteAddress = exchange.getIn().getHeader(NettyConstants.NETTY_REMOTE_ADDRESS, InetSocketAddress.class);
            session.callerAddress(nettyRemoteAddress.getAddress().getHostAddress());
            NettyHttpMessage httpMessage = exchange.getIn().getBody(NettyHttpMessage.class);
            session.requestedOperation(httpMessage.getHttpRequest().method().toString()
                    + " " + httpMessage.getHttpRequest().uri()
                    + " " + httpMessage.getHttpRequest().protocolVersion().text());
            session.userAgent(exchange.getIn().getHeader("User-Agent", "Unknown User-Agent", String.class));
        } catch(Exception e) {
            // unable to manage netty
        }
        exchange.setProperty("session", session);
        return session;
    }

    default void preprocessExchange(Exchange exchange) throws IOException {
        String contentType = exchange.getIn().getHeader("Content-Type", "", String.class);
        if (exchange.getIn() instanceof NettyHttpMessage && !contentType.isEmpty() && contentType.contains("multipart/form-data")) {

            HttpPostRequestDecoder request = new HttpPostRequestDecoder(exchange.getIn(NettyHttpMessage.class).getHttpRequest());

            for (InterfaceHttpData part : request.getBodyHttpDatas()) {
                processHttpPart(exchange, part);
            }
            exchange.getIn().setHeader(Exchange.HTTP_SERVLET_REQUEST, request);
        } else if (exchange.getIn().getBody() instanceof Map) {
            exchange.getIn().getBody(HashMap.class).forEach((key, value) -> {
                exchange.getIn().setHeader((String) key, value);
            });
        }

        // TODO move this workaround in a better place ?

        if(exchange.getIn().getHeader("CamelHttpPath",String.class).contains("%2F")) {
            String endpointUri = exchange.getFromEndpoint().getEndpointUri();
            endpointUri = endpointUri.substring(0, endpointUri.indexOf('?'));
            String[] splittedEndpointUri = endpointUri.split("/");
            String[] splittedHttpUrl = exchange.getIn().getHeader("CamelHttpUrl", String.class).split("/");
            for(int i =0; i < splittedEndpointUri.length; i++) {
                if(splittedEndpointUri[i].contains("%7B")) {
                    String name = splittedEndpointUri[i].replace("%7B","").replace("%7D","");
                    exchange.getIn().setHeader(name, splittedHttpUrl[i]);
                }
            }
        }

        exchange.getIn().getHeaders().forEach((key, value) -> {
            if (value instanceof String) {
                try {
                    exchange.getIn().setHeader(key, URLDecoder.decode((String) value, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        if (exchange.getIn().hasAttachments()) {
            exchange.getIn().getAttachments().forEach((key, value) -> exchange.getIn().setHeader(key, value.getDataSource()));
        }
    }

    default void processHttpPart(Exchange exchange, InterfaceHttpData part) throws IOException {
        if (part instanceof MixedAttribute) {
            Attribute attribute = (MixedAttribute) part;
            exchange.getIn().setHeader(attribute.getName(), URLDecoder.decode(attribute.getValue(), "UTF-8"));
        } else if (part instanceof MixedFileUpload) {
            MixedFileUpload attribute = (MixedFileUpload) part;

            if (attribute.isCompleted()) {
                FileDataSource fileDataSource;
                if (attribute.isInMemory()) {
                    File file = new File(System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID() + "_" + attribute.getFilename());
                    fileDataSource = new FileDataSource(file);
                    Files.write(attribute.get(), file);
                } else fileDataSource = new FileDataSource(attribute.getFile());
                exchange.getIn().addAttachment(attribute.getName(), new DataHandler(fileDataSource));
            }
        }
    }

    default void postprocessExchange(Exchange exchange) throws IOException {
        if (exchange.getIn().getBody() != null && exchange.getIn().getBody() instanceof FileDataSource) {
            handleFileExchange(exchange);
        }
        HttpPostRequestDecoder httprequest = exchange.getIn().getHeader(Exchange.HTTP_SERVLET_REQUEST, HttpPostRequestDecoder.class);
        if (httprequest != null) {
            httprequest.destroy();
        }
        exchange.getIn().removeHeader(Exchange.HTTP_SERVLET_REQUEST);
        if (exchange.getIn().hasAttachments()) {
            exchange.getIn().getAttachments().forEach((key, value) -> {
                exchange.getIn().removeHeader(key);
                FileDataSource fds = (FileDataSource) value.getDataSource();
                FileUtils.deleteQuietly(fds.getFile());
            });
            exchange.getIn().setAttachments(null);
        }
    }

    default void handleFileExchange(Exchange exchange) throws IOException {
        FileDataSource body = exchange.getIn().getBody(FileDataSource.class);
        if (body.getName().endsWith(".png")) {
            exchange.getOut().setHeader("Content-Type", "image/png");
        } else {
            exchange.getOut().setHeader("Content-Disposition", "attachment;filename=" + body.getName());
        }
        exchange.getOut().setBody(IOUtils.toByteArray(body.getInputStream()));
    }

    default void handleRequest(Exchange exchange) throws Exception {
        preprocessExchange(exchange);
        Session session = createNewSession(exchange);
        handleSession(session);
        session.authenticated(false);
        session.setCallerName("Anonymous user");
        LogUtils.getLogger(exchange).info("{} - {} \"{}\" {}", session.getCallerAddress(), session.getCallerName(), session.getRequestedOperation(), session.getUserAgent());
    }

    default void postProcessResponse(Exchange exchange) throws IOException {
        exchange.getIn().removeHeaders("*");
        postprocessExchange(exchange);
        deleteSession();
    }

    default boolean checkParameter(String name, Object value) throws KathraException {
        if (value == null) {
            throw new KathraException(name + " is null").errorCode(KathraException.ErrorCode.BAD_REQUEST);
        } else if (String.class.isInstance(value.getClass()) && ((String) value).isEmpty()) {
            throw new KathraException(name + " is empty").errorCode(KathraException.ErrorCode.BAD_REQUEST);
        } else {
            return true;
        }
    }
}
