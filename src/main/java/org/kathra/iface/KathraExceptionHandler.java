/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.iface;

import org.kathra.utils.ApiException;
import org.kathra.utils.KathraApiResponse;
import org.kathra.utils.KathraException;
import org.kathra.utils.serialization.GsonUtils;
import org.apache.camel.Exchange;
import org.slf4j.Logger;

import java.net.ConnectException;
import java.util.MissingResourceException;
import java.util.UUID;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public interface KathraExceptionHandler {
    default void handleException(Exchange exchange) {
        String controllerName = ExchangeUtils.getControllerNameFromExchange(exchange);
        String exceptionId = controllerName + "Exception-" + UUID.randomUUID().toString();
        Logger logger = LogUtils.getLogger(exchange);
        Exception ex = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
        logger.info(exceptionId, ex);
        KathraApiResponse response = new KathraApiResponse();
        if (ex instanceof KathraException) {
            KathraException kathraException = (KathraException) ex;
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, kathraException.getErrorCode().getCode());
            response.message(exceptionId + ": " + kathraException.getMessage())
                    .statusCode(KathraApiResponse.HttpStatusCode.valueOf(kathraException.getErrorCode().name()));
        } else if (ex instanceof MissingResourceException) {
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, "404");
            response.message(ex.getMessage()).statusCode(KathraApiResponse.HttpStatusCode.NOT_FOUND);
        } else if (ex instanceof ApiException) {
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, ((ApiException) ex).getCode());
            ApiException apiEx = (ApiException) ex;
            response.message(ex.getMessage());
            response.statusCode(KathraApiResponse.HttpStatusCode.getEnumFromCode(apiEx.getCode()));
            if (apiEx.getCode() == 404) {
                response.statusCode(KathraApiResponse.HttpStatusCode.NOT_FOUND);
            } else if (apiEx.getResponseBody() != null) {
                response = GsonUtils.gson.fromJson(((ApiException) ex).getResponseBody(), KathraApiResponse.class);
            } else if (apiEx.getCause() instanceof ConnectException) {
                response.statusCode(KathraApiResponse.HttpStatusCode.GATEWAY_TIMEOUT).message(apiEx.getMessage());
                exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, "504");
            }
        } else {
            response.message(exceptionId + ": " + ex.toString()).statusCode(KathraApiResponse.HttpStatusCode.INTERNAL_SERVER_ERROR);
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, response.getStatusCode().getCode());
        }
        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
        exchange.getOut().setBody(GsonUtils.gson.toJson(response));
    }

}
