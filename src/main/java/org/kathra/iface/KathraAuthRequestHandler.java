/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.iface;

import org.kathra.utils.Session;
import org.kathra.utils.security.KeycloakUtils;
import org.apache.camel.Exchange;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public interface KathraAuthRequestHandler extends KathraRequestHandler {

    default void handleAuthenticatedRequest(Exchange exchange) throws Exception {
        preprocessExchange(exchange);
        Session session = createNewSession(exchange);
        session.authenticated(false);
        handleSession(session);
        KeycloakUtils.handleAuthentication(session, LogUtils.getLogger(exchange), exchange.getIn().getHeader("Authorization","",String.class));
        LogUtils.getLogger(exchange).info("{} - {} \"{}\" {}", session.getCallerAddress(), session.getCallerName(), session.getRequestedOperation(), session.getUserAgent());
    }
}
